package sc.fiji.cellCounter;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JRadioButton;

import ij.IJ;

public class Teclado implements KeyListener {

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		System.out.println("Pressionou uma tecla");
		IJ.log("PRESSIONOU");

		int id = e.getID();
		String keyString = null;
		int currentMarkerIndex = 0;
		char c = e.getKeyChar();
		IJ.log("key character = '" + c + "'");

		int key = e.getKeyCode();

		if (key == KeyEvent.VK_A) {
			JRadioButton rb = CellCounter.dynRadioVector.get(0);
			rb.setSelected(true);
			rb.doClick();
			currentMarkerIndex = 0;
			c = e.getKeyChar();
			keyString = "key character = '" + c + "'";
		} else if (key == KeyEvent.VK_S) {
			JRadioButton rb = CellCounter.dynRadioVector.get(1);
			rb.setSelected(true);
			rb.doClick();
			currentMarkerIndex = 1;
			c = e.getKeyChar();
			keyString = "key character = '" + c + "'";
		} else if (key == KeyEvent.VK_D) {
			JRadioButton rb = CellCounter.dynRadioVector.get(2);
			rb.setSelected(true);
			rb.doClick();
			currentMarkerIndex = 2;
			c = e.getKeyChar();
			keyString = "key character = '" + c + "'";
		} else if (key == KeyEvent.VK_F) {
			JRadioButton rb = CellCounter.dynRadioVector.get(3);
			rb.setSelected(true);
			rb.doClick();
			currentMarkerIndex = 3;
			c = e.getKeyChar();
			keyString = "key character = '" + c + "'";
		}

		IJ.log(keyString);

		System.out.println("Tecla no intervalo: " + keyString);

		System.out.println("currentMarkerIndex = " + currentMarkerIndex);

		// ic.setDelmode(false); // just in case
		CellCounter.currentMarkerVector = CellCounter.typeVector.get(currentMarkerIndex);

		System.out.println("currentMarkerVector = " + CellCounter.currentMarkerVector.getName());

		CellCounter.ic.setCurrentMarkerVector(CellCounter.currentMarkerVector);
		CellCounter.ic.repaint();
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

}
